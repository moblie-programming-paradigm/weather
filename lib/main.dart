import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.pink,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Colors.black,
        foregroundColor: Colors.white,
      ),
      listTileTheme: ListTileThemeData(
        iconColor: Colors.pink,
      ),
    );
  }
  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black12,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      listTileTheme: ListTileThemeData(
        iconColor: Colors.white,
      ),
    );
  }
}

class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget buildWeather1() {
  return Column(
    children: <Widget>[
      Text("ตอนนี้"),
      Text(""),
      Icon(
        Icons.cloud,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("25°"),
    ],
  );
}
Widget buildWeather2() {
  return Column(
    children: <Widget>[
      Text("23:00"),
      Text(""),
      Icon(
        Icons.cloud,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("24°"),
    ],
  );
}
Widget buildWeather3() {
  return Column(
    children: <Widget>[
      Text("00:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("23°"),
    ],
  );
}
Widget buildWeather4() {
  return Column(
    children: <Widget>[
      Text("01:00"),
      Text(""),
      Icon(
        Icons.thunderstorm,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("24°"),
    ],
  );
}
Widget buildWeather5() {
  return Column(
    children: <Widget>[
      Text("02:00"),
      Text(""),
      Icon(
        Icons.cloud,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("26°"),
    ],
  );
}
Widget buildWeather6() {
  return Column(
    children: <Widget>[
      Text("03:00"),
      Text(""),
      Icon(
        Icons.cloud,
        // color: Colors.indigo.shade800,
      ),
      Text(""),
      Text("25°"),
    ],
  );
}

//ListTile
Widget TodayListTile() {
  return ListTile(
    title: Text("วันนี้"),
    subtitle: Text("24° / 32°"),
    trailing: Icon(Icons.thunderstorm),
  );
}
Widget TomorrowListTile() {
  return ListTile(
    title: Text("พฤ."),
    subtitle: Text("29° / 35°"),
    trailing: Icon(Icons.sunny),
  );
}
Widget FridayListTile() {
  return ListTile(
    title: Text("ศ."),
    subtitle: Text("23° / 29°"),
    trailing: Icon(Icons.cloud),
  );
}
Widget SaturdayListTile() {
  return ListTile(
    title: Text("ส."),
    subtitle: Text("20° / 27°"),
    trailing: Icon(Icons.cloud),
  );
}
Widget SundayListTile() {
  return ListTile(
    title: Text("อา."),
    subtitle: Text("22° / 228°"),
    trailing: Icon(Icons.cloud),
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    // backgroundColor: Colors.white,
    leading: IconButton(
      icon: Icon(Icons.menu),
      // color: Colors.black,
      onPressed: (){
        print("Menu");
      },
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.settings),
        // color: Colors.black,
        onPressed: (){
          print("Settings");
        },
      ),
    ],
  );
}
Widget buildBodyWidget() {
  return ListView (
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              //Height constraint at Container widget level
              height: 400,
              child: Image.network(
                "https://i.imgur.com/LDG1v9c.jpg",
                fit: BoxFit.fill,
              ),
            ),
            Text(""),
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child : Theme (
                data: ThemeData(
                    iconTheme: IconThemeData (
                      color: Colors.white,
                    )
                ),
                child: profileActionItems(),
              ),
            ),
            Divider(
              color: Colors.grey,
            ),
            TodayListTile(),
            TomorrowListTile(),
            FridayListTile(),
            SaturdayListTile(),
            SundayListTile(),

          ],
        )
      ]
  );
}
Widget profileActionItems() {
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildWeather1(),
        buildWeather2(),
        buildWeather3(),
        buildWeather4(),
        buildWeather5(),
        buildWeather6(),
      ]
  );
}
